<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/login', 'PagesController@login');

Route::resources([
    'users' => 'UsersController',
    'cathegories' => 'CathegoriesController',
    'products' => 'ProductsController',
    'roles' => 'RolesController',
    'users' => 'UsersController'
]);

Auth::routes();


Route::get('/basket', 'BasketController@index');

Route::get('/basket/{id}', ['uses' => 'BasketController@add',
                            'as' => 'basket.add'])
                            ->where(['id' => '[0-9]+']);

Route::get('/basket/store', 'BasketController@store');

Route::get('/orders', 'OrdersController@index');