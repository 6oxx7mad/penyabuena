@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1>cathegories</h1>

    @unless (Auth::check())
    @else 
        <p>

            <a href="/cathegories/create">Create</a>

        </p>
    @endunless
    
    
    @if($cathegories->count())

        <table class="table">

        <thead>

            <tr>

                <th><abbr title="Id">Id</abbr></th>
                <th><abbr title="Name">Name</abbr></th>

            </tr>

        </thead>


        <tfoot>

            <tr>
                <th>id</th>
                <th>Name</th>

            </tr>

        </tfoot>


        <tbody>
        
            @foreach ($cathegories as $cathegory)

                
                <tr>

                <th>{{ $cathegory->id }}</th>
                
                <td><a href="/cathegories/{{ $cathegory->id }}">{{ $cathegory->name }}</a></td>

                </tr>

            @endforeach

        </tbody>


        </table>

    @endif

@endsection
