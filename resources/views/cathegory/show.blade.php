@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1 class="title">{{ $cathegory->name }}</h1>

    @unless (Auth::check())
    @else 

        <p>

            <a href="/categories/{{ $cathegory->id }}/edit">Edit</a>

        </p>

    @endunless
    
    <div>Products: </div>

    @if($cathegory->products->count())

        <table class="table">

            <thead>

                <tr>

                    <th><abbr title="Id">Id</abbr></th>
                    <th><abbr title="Name">Name</abbr></th>
                    <th><abbr title="Price">Price</abbr></th>

                </tr>

            </thead>


            <tfoot>

                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Price</th>

                </tr>

            </tfoot>


            <tbody>

                @foreach ($cathegory->products as $product)

                    
                    <tr>

                    <th>{{ $product->id }}</th>
                    
                    <td><a href="/products/{{ $product->id }}">{{ $product->name }}</a></td>
                    <td>{{ $product->price }}</td>

                    </tr>

                @endforeach

            </tbody>


        </table>

    @endif

@endsection
