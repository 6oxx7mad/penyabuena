@extends('template.layout')


@section('title', 'Pena')


@section('content')

@if('Administrador' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name or 'root' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name)

        <h1 class="title">Edición de {{ $cathegory->name }}</h1>
        
        <form method="POST" action="/cathegories/{{ $cathegory->id }}" style="margin-bottom: 1rem;">

            @method('PATCH');


            @csrf

            <div class="field">

                <label class="label" for="name">Name:</label>

                <div class="control">

                    <input type="text" name="name" placeholder="name" value="{{ $cathegory->name }}">

                </div>

            </div>

            
            <div class="field">

                <div class="control">

                    <button type="submit" class="button is-link">Edit</button>

                </div>  

            </div>

        </form>

        <form method="POST" action="/cathegories/{{ $cathegory->id }}">

            @method('DELETE')


            @csrf

            <div class="field">

                <div class="control">

                    <button type="submit" class="button is-danger">Delete</button>

                </div>  

            </div>

        </form>

        @else

            <h1 class="title">No Tienes suficientes permisos</h1>

            <p>

                <a href="{{ URL::previous() }}">Back</a>

            </p>

        @endif


@endsection
