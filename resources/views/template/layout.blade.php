<!DOCTYPE html>
<html lang="es">
<head>
    <title>@yield('title', 'Pena Edition')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>

    <nav class="navbar" role="navigation" aria-label="main navigation">

        <div class="navbar-menu">

            <div class="navbar-start">
            <a href="/" class="navbar-item">
                Home
            </a>


            <div class="navbar-item has-dropdown is-hoverable">
                
                <a class="navbar-link">
                    More
                </a>


                <div class="navbar-dropdown">

                    <a href="/users" class="navbar-item">
                        Users
                    </a>

                    <a href="/products" class="navbar-item">
                        Products
                    </a>


                    <a href="/cathegories" class="navbar-item">
                        Categories
                    </a>

                    <a href="/roles" class="navbar-item">
                        Roles
                    </a>

                    <hr class="navbar-divider">

                    <a class="navbar-item">
                        MORE COMING SOON
                    </a>

                </div>


            </div>

            <div>

            <i class="fas fa-shopping-cart" style="margin-top: 1rem;"><a href="/basket">Basket</a></i>

            </div>

            </div>

            <!-- Authentication Links -->
            @guest


            <div class="navbar-end">

                <div class="navbar-item">

                    <div class="buttons" style="margin-right: 1rem">

                        <a class="button is-light" href="{{ route('login') }}">{{ __('Login') }}</a>

                        @if (Route::has('register'))


                        <a class="button is-primary" href="{{ route('register') }}">{{ __('Register') }}</a>


                        @endif


                        @else

                        <div class="navbar-item has-dropdown is-hoverable" style="margin-right: 2rem;">
        
                                <a id="navbarDropdown" class="navbar-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="navbar-dropdown">

                                    <a class="navbar-item" href="/users/{{ Auth::user()->id }}/edit">
                                            Edit
                                    </a>

                                    <a class="navbar-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                            
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </div>

                        </div>
                        
                        @endguest

                    </div>

                </div>

            </div>

        </div>
    </nav>

    @yield('content')
</body>
</html>