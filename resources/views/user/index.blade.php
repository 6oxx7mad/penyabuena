@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1>Users</h1>

    <!--<p>

        <a href="/users/create">Create</a>

    </p>-->
    
    @if($users->count())

        <table class="table">

        <thead>

            <tr>

                <th><abbr title="Id">Id</abbr></th>
                <th><abbr title="Name">Name</abbr></th>
                <th><abbr title="Email">Email</abbr></th>
                <th><abbr title="Role">Role</abbr></th>

            </tr>

        </thead>


        <tfoot>

            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>

            </tr>

        </tfoot>


        <tbody>
        
            @foreach ($users as $user)

                
                <tr>

                <th>{{ $user->id }}</th>
                
                <td><a href="/users/{{ $user->id }}">{{ $user->name }}</a></td>
                <td>{{ $user->email }}</td>
                <td><a href="/roles/{{ $user->role->id }}">{{ $user->role->name }}</a></td>

                </tr>

            @endforeach

        </tbody>


        </table>

    @endif

@endsection
