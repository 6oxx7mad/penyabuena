@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1 class="title">{{ $user->name }}</h1>
    
    <div>Email: </div>

    <div class="content">{{ $user->email }}</div>
    
    <div>Role: </div>

    <div class="content">{{ $user->role->name }}</div>

    <p>

        <a href="{{ URL::previous() }}">Back</a>

    </p>

@endsection
