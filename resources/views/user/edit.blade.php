@extends('template.layout')


@section('title', 'Pena')


@section('content')

    <h1 class="title">Edit Profile: {{ $theUser[0]->name }}</h1>
    
    <form method="POST" action="/users/{{ $theUser[0]->id }}" style="margin-bottom: 1rem;">

        @method('PATCH')


        @csrf

        <div class="field">

            <label class="label" for="name">Name:</label>

            <div class="control">

                <input type="text" name="name" placeholder="name" value="{{ $theUser[0]->name }}">

            </div>

        </div>

        <div class="field">

            <label class="label" for="email">Email:</label>

            <div class="control">

                <input type="text" name="email" placeholder="email" value="{{ $theUser[0]->email }}">

            </div>

        </div>
        
        <div class="field">

            <div class="control">

                <button type="submit" class="button is-link">Edit</button>

            </div>  

        </div>

    </form>


@endsection
