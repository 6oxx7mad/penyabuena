@extends('template.layout')

@section('title', 'Pena')

@section('content')

    <h1>Basket</h1>

    @if(Session::has('basket'))

        <ul>

            @foreach($basket->products as $product)

                <span>Name: </span>

                <li>{{ $product['product']->name}}</li>

                <span>Quantity: </span>

                <li>{{ $product['qty'] }}</li>

                <span>Price: </span>

                <li>{{ $product['price'] }}</li>


            @endforeach

        </ul>

        <span>Total price: {{ $basket->totalPrice }}</span>
        <span>Total Quantity: {{ $basket->totalQty }}</span>

        <div class="control">
            
            <a class="tag is-primary" href="/basket/store">Save it</a>

        </div>

    @else

        <span>No hay productos en la cesta</span>

    @endif

@endsection
