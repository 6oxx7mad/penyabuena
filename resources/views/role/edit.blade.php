@extends('template.layout')


@section('title', 'Pena')


@section('content')

    @if('admin' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name)


        <h1 class="title">Edición de {{ $role->name }}</h1>
        
        <form method="POST" action="/roles/{{ $role->id }}" style="margin-bottom: 1rem;">

            @method('PATCH');


            @csrf

            <div class="field">

                <label class="label" for="name">Name:</label>

                <div class="control">

                    <input type="text" name="name" placeholder="name" value="{{ $role->name }}">

                </div>

            </div>

            
            <div class="field">

                <div class="control">

                    <button type="submit" class="button is-link">Edit</button>

                </div>  

            </div>

        </form>

    @else

        <h1 class="title">No Tienes suficientes permisos</h1>

    @endif

@endsection
