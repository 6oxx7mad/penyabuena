@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1>Roles</h1>
    
    @if($roles->count())

        <table class="table">

        <thead>

            <tr>

                <th><abbr title="Id">Id</abbr></th>
                <th><abbr title="Name">Name</abbr></th>

            </tr>

        </thead>


        <tfoot>

            <tr>
                <th>id</th>
                <th>Name</th>

            </tr>

        </tfoot>


        <tbody>
        
            @foreach ($roles as $role)

                
                <tr>

                <th>{{ $role->id }}</th>
                
                <td><a href="/roles/{{ $role->id }}">{{ $role->name }}</a></td>

                </tr>

            @endforeach

        </tbody>


        </table>

    @endif

@endsection
