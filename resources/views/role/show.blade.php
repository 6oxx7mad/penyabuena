@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1 class="title">{{ $role->name }}</h1>

    <p>

        <a href="/roles/{{ $role->id }}/edit">Edit</a>

    </p>
    
    <div>Products: </div>

    @if($role->users->count())

        <table class="table">

            <thead>

                <tr>

                    <th><abbr title="Id">Id</abbr></th>
                    <th><abbr title="Name">Name</abbr></th>
                    <th><abbr title="Email">Email</abbr></th>

                </tr>

            </thead>


            <tfoot>

                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>

                </tr>

            </tfoot>


            <tbody>

                @foreach ($role->users as $user)

                    
                    <tr>

                    <th>{{ $user->id }}</th>
                    
                    <td><a href="/users/{{ $user->id }}">{{ $user->name }}</a></td>
                    <td>{{ $user->email }}</td>

                    </tr>

                @endforeach

            </tbody>


        </table>

    @endif

@endsection
