@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1 class="title">{{ $product->name }}</h1>
    
    <div>Price: </div>

    <div class="content">{{ $product->price }}</div>
    
    <div>Category: </div>

    <div class="content">{{ $product->category->name }}</div>

    <span class="tag is-dark">

        <a href="/products/{{ $product->id }}/edit">Edit</a>

    </span>

    <span class="tag is-dark">

        <a href="{{ URL::previous() }}">Back</a>

    </span>

@endsection
