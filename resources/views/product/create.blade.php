@extends('template.layout')

@section('title', 'Pena')

@section('content')

    @if('Administrador' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name or 'root' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name)

        <h1>Create Producto</h1>
        
        <form method="POST" action="/products">

            {{ csrf_field() }}

            <div>

                <input type="text" name="name" placeholder="Product Name">

            </div>


            <div>

                <input type="number" name="price" placeholder="Product Price">

            </div>


            <select name="category_id">

            @foreach ($categories as $category)

                <option value="{{ $category->id }}">
                    {{ $category->name }}
                </option>

            @endforeach

            </select>
            

            <div>

                <button type="submit">Create Product</button>

            </div>

        </form>

    @else

        <h1 class="title">No Tienes suficientes permisos</h1>

    @endif
    
@endsection
