@extends('template.layout')


@section('title', 'Pena')


@section('content')


@if('Administrador' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name or 'root' == App\Role::findOrFail(App\User::findOrFail(auth()->id())->role_id)->name)

        <h1 class="title">Edición de {{ $product->name }}</h1>
        
        <form method="POST" action="/products/{{ $product->id }}" style="margin-bottom: 1rem;">

            @method('PATCH');


            @csrf

            <div class="field">

                <label class="label" for="name">Name:</label>

                <div class="control">

                    <input type="text" name="name" placeholder="name" value="{{ $product->name }}">

                </div>

            </div>

            <div class="field">

                <label class="label" for="price">Price:</label>

                <div class="control">

                    <input type="number" name="price" placeholder="price" value="{{ $product->price }}">

                </div>

            </div>

            <div class="field">

                <div class="control">

                    <div class="select is-primary">

                        <select name="category_id">


                        @foreach ($categories as $category)

                            <option value="{{ $category->id }}" 
                                        
                                {{ $category->id == $product->category_id ? 'selected="selected"' : '' }}
                                >{{ $category->name }}

                            </option>

                        @endforeach


                        </select>

                    </div>

                </div>

            </div>
            
            <div class="field">

                <div class="control">

                    <button type="submit" class="button is-link">Edit</button>

                </div>  

            </div>

        </form>

        <form method="POST" action="/products/{{ $product->id }}">

            @method('DELETE')


            @csrf

            <div class="field">

                <div class="control">

                    <button type="submit" class="button is-danger">Delete</button>

                </div>  

            </div>

        </form>

    @else

        <h1 class="title">No Tienes suficientes permisos</h1>

        <p>

            <a href="{{ URL::previous() }}">Back</a>

        </p>

    @endcan

@endsection
