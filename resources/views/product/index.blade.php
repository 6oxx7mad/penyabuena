@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1>Products</h1>

    <p>

        <a href="/products/create">Create</a>

    </p>
    
    @if($products->count())

        <table class="table">

        <thead>

            <tr>

                <th><abbr title="Id">Id</abbr></th>
                <th><abbr title="Name">Name</abbr></th>
                <th><abbr title="Price">Price</abbr></th>
                <th><abbr title="Category">Category</abbr></th>
                <th><abbr title="Buy">Buy</abbr></th>

            </tr>

        </thead>


        <tfoot>

            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>Buy</th>

            </tr>

        </tfoot>


        <tbody>
        
            @foreach ($products as $product)

                
                <tr>

                <th>{{ $product->id }}</th>
                
                <td><a href="/products/{{ $product->id }}">{{ $product->name }}</a></td>
                <td>{{ $product->price }}</td>
                <td><a href="/cathegories/{{ $product->cathegory->id }}">{{ $product->cathegory->name }}</a></td>
                <td><a class="tag is-dark" href="/basket/{{ $product->id }}">Buy it</a></td>

                </tr>

            @endforeach

        </tbody>


        </table>

    @endif

@endsection
