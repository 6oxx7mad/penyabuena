@extends('template.layout')

@section('title', 'Pena')

@section('content')


    <h1>Orders</h1>

    @unless (Auth::check())
    @else 

    
        <table class="table">

        <thead>

            <tr>

                <th>Show</th>
                <th><abbr title="Paid">Paid</abbr></th>
                <th><abbr title="Date">Date</abbr></th>
                <th><abbr title="User">User</abbr></th>

            </tr>

        </thead>


        <tfoot>

            <tr>
                <th>Show</th>
                <th>Paid</th>
                <th>Date</th>
                <th>User</th>

            </tr>

        </tfoot>


        <tbody>
        
            @foreach ($orders as $order)

                
                <tr>

                <th> <a href="/orders/{{ $order->id }}">The Order</a> </th>

                <td> {{ $order->paid == 1 ? "Paid" : "No paid" }} </td>
                
                <td> {{ $order->date }} </td>

                <td> {{ $order->user->name }} </td>

                </tr>

            @endforeach

        </tbody>


        </table>

        <h1>Total Price: {{$globalPrice }}</h1>

    @endunless

@endsection
