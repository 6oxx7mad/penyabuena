<?php

namespace App\Http\Controllers;

use App\Product;
use App\Role;
use App\Cathegory;
use App\User;
use Auth;

use Illuminate\Http\Request;

class ProductsController extends Controller

{

    public function __construct()

    {

        $this->middleware('auth');
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {

        $products = Product::all();

        return view('product.index', compact('products'));    

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        //dd(Role::findOrFail(User::findOrFail(auth()->id())->role_id)->name);

        $categories = Category::all();

        return view('product.create', compact('cathegories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {

        Product::create(request(['name', 'price', 'cathegory_id']));

        return redirect('/products');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)

    {
        
        return view('product.show', compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)

    {

        $categories = Category::all();

        return view('product.edit', compact('product', 'cathegories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    
    {
        
        $product->update(request(['name', 'price', 'category_id']));

        return redirect('/products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)

    {
        
        $product->delete();

        return redirect('/products');
        
    }
}
