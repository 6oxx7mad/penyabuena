<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Role;
use App\User;

class OrdersController extends Controller

{

    public function __construct()

    {

        $this->middleware('auth');

    }



    public function index(Request $request)

    {

        $canDo = ['admin', 'root', 'administrador'];
        
        if(in_array(Role::findOrFail(User::findOrFail(auth()->id())->role_id)->name, $canDo) && $request->get('all') == 1) {

            $globalPrice = Order::totalGlobal();
            $orders = Order::paginate(10);
    
            return view('order.index', compact('orders', 'globalPrice'));  

        }elseif (in_array(Role::findOrFail(User::findOrFail(auth()->id())->role_id)->name, $canDo) && $request->get('all') == 0) {
            $globalPrice = Order::total();
            $orders = Order::where('user_id', '=', auth()->id())->paginate(10);
    
            return view('order.index', compact('orders', 'globalPrice'));  
        }elseif (!in_array(Role::findOrFail(User::findOrFail(auth()->id())->role_id)->name, $canDo)) {
            $globalPrice = Order::total();
            $orders = Order::where('user_id', '=', auth()->id())->paginate(10);

            $noAuth = "No";
    
            return view('order.index', compact('orders', 'globalPrice', 'noAuth'));  
        }
        
    }

}
