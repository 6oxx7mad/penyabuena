<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class PagesController extends Controller

{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()

    {

        $this->middleware('auth');
        
    }

    public function home() 
    
    {

        return view('welcome');

    }

    public function login()

    {

        return view('login');

    }

}
