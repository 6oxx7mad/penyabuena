<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\User;
use App\Basket;
use App\Order;
use Session;
use Auth;


class BasketController extends Controller

{

    public function __construct()

    {

        $this->middleware('auth');
        
    }

    public function index()
    
    {

        if(!Session::has('basket'))
        {

            return view('basket.index');

        }

        $oldBasket = Session::get('basket');

        $basket = new Basket($oldBasket);

        return view('basket.index', compact('basket'));

    }
    
    public function add(Request $request, $id)
    
    {
        
        $product = Product::findOrFail($id);

        $oldBasket = Session::has('basket') ? Session::get('basket') : null;

        $basket = new Basket($oldBasket);
        $basket->add($product);

        $request->session()->put('basket', $basket);

        //$request->session()->flush();

        return redirect()->action('BasketController@index');

    }

    public function store(Request $request)

    {

        if(!Session::has('basket'))
        {

            return view('basket.index');

        }

        $oldBasket = Session::get('basket');

        $basket = new Basket($oldBasket);

        $order = DB::table('orders')->insert(['paid' => '0', 'date' => now()->format('Y-m-d H:i:s'), 'user_id' => Auth::id()]);

        $last = Order::latest()->first();

        foreach ($basket->products as $product)
        {

            DB::table('order_product')->insert(['order_id' => $last->id, 'product_id' => $product['product']->id, 'quantity' => $product['qty'], 'price' => $product['price']]);

        }

        $request->session()->pull('basket');

        return redirect('/');

    }

}
