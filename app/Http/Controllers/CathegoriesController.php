<?php

namespace App\Http\Controllers;

use App\cathegory;
use App\Role;
use Auth;
use Illuminate\Http\Request;

class CathegoriesController extends Controller

{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    
    {
        
        $cathegories = cathegory::all();

        return view('cathegory.index', compact('cathegories'));  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('cathegory.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        cathegory::create(request(['name']));

        return redirect('/cathegories');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function show(cathegory $cathegory)

    {
        
        return view('cathegory.show', compact('cathegory'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function edit(cathegory $cathegory)
    {

        return view('cathegory.edit', compact('cathegory'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cathegory $cathegory)
    {
        
        $cathegory->update(request(['name']));

        return redirect('/cathegories');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cathegory  $cathegory
     * @return \Illuminate\Http\Response
     */
    public function destroy(cathegory $cathegory)
    {
        
        $cathegory->delete();

        return redirect('/cathegories');

    }
}
