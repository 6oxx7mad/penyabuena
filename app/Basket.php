<?php

namespace App;

use App\Product;

class Basket

{
    
    public $products = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)

    {

        if($oldCart)
        {

            $this->products = $oldCart->products;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;

        }

    }

    public function add(Product $product)

    {

        $storedProduct = ['qty' => 0, 'price' => $product->price, 'product' => $product];
        
        if($this->products) 
        {
            if(array_key_exists($product->id, $this->products))
            {
                $storedProduct = $this->products[$product->id];
            }
        }

        $storedProduct['qty']++;
        $storedProduct['price'] = $product->price * $storedProduct['qty'];
        $this->products[$product->id] = $storedProduct;
        $this->totalQty++;
        $this->totalPrice += $product->price;

    }

}
