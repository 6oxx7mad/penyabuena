<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Order extends Model

{
    

    protected $fillable = [
        'paid', 'date', 'user_id'
    ];

    public function user() 
    
    {

        return $this->belongsTo(User::class);

    }

    public static function totalGlobal()

    {

        $total = 0;

        $totales = DB::table('order_product')->pluck('price');

        foreach($totales as $aux)
        {

            $total += $aux;

        }

        return $total;

    }

    public static function total()

    {

        $total = 0;

        $totales = DB::table('orders')->rightJoin('order_product', 'orders.id', '=', 'order_product.order_id')->pluck('price');    

        //dd($totales);

        foreach($totales as $aux)
        {

            $total += $aux;

        }

        return $total;

    }


}
