<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract


{

use Authenticatable, Authorizable, CanResetPassword;

    
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    public function role() 
    
    {

        return $this->belongsTo(Role::class);

    }


    public function orders()
    
    {

        return $this->hasMany(Order::class);

    }

}
